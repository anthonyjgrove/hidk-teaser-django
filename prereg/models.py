from django.db import models
import mailchimp
import logging
from django.conf import settings
from django.forms import ModelForm
from django.dispatch import receiver


# Create your models here.
class Member(models.Model):
    mem_email = models.CharField("Email", max_length=65)
    join_at = models.DateTimeField("Date Joined", auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
       return self.mem_email

    class Meta:
       verbose_name_plural = "Members"

@receiver(models.signals.post_save, sender=Member)
def execute_after_save(sender, instance, created, *args, **kwargs):
    if created:
       list_name = settings.MAILCHIMP_LIST
       action = 'subscribe'
       email = instance.mem_email
       logger = logging.getLogger(__name__)
       logger.error('2')
       m = mailchimp.Mailchimp(settings.MAILCHIMP_KEY)
       try:
          m.helper.ping()
       except mailchimp.Error:
          logger.error('Invalid Mailchimp API key')
          return
    
       try:
          lists = m.lists.list({'list_name': list_name})
          lst = lists['data'][0]
       except mailchimp.ListDoesNotExistError:
          logger.error('Non-existent list requested from Mailchimp')
          return
       except mailchimp.Error, e:
          logger.error('An error occurred with Mailchimp API: %s - %s' % (e.__class__, e))
          return
       else:
          try:
             if action == 'subscribe':
                sub = m.lists.subscribe(lst['id'], {'email': email}, double_optin=False)
             elif action == 'unsubscribe':
                unsub = m.lists.unsubscribe(lst['id'], {'email': email})
          except mailchimp.ListAlreadySubscribedError:
             logger.error('Email is already subscribed')
          except mailchimp.ListNotSubscribedError:
             logger.error('Email is not subscribed')
          except mailchimp.Error, e:
             logger.error('An error occurred with Mailchimp API: %s - %s' % (e.__class__, e))

class MemberForm(ModelForm):
    class Meta:
         model = Member
         fields = ['mem_email',]