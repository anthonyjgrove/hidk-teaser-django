from django.contrib import admin
from .models import Member


class MemberAdmin(admin.ModelAdmin):
    fields = ('mem_email', 'join_at',)
    readonly_fields = ('join_at', 'updated_at')
    list_display = ('mem_email','join_at',)
admin.site.register(Member, MemberAdmin)

# Register your models here.
