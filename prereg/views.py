from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
import prereg.models as preregmodels
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView


# Create your views here.

class ShareView(TemplateView):
    template_name = 'share.html'


class LandingView(CreateView):
    model = preregmodels.Member
    template_name = 'index.html'
    fields =['mem_email',]

    def get_success_url(self):
       return 'share.html'