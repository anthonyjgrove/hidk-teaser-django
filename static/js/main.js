var main = function(){
    $("#submit-button").hide();
    $(".email-box").focus(function() {
        $("#submit-button").show();
        $(this).attr('style', 'width: 100% !important;');
        $(".input-field label").attr('style', 'left: 0.75rem!important;');
    });
};
$(document).ready(main);